<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	$dbresult = $this;

	if(intval($dbresult->NavPageSize) <= 0)
		$dbresult->NavPageSize = 10;

	$arResult = Array();

	$arResult["NavShowAlways"] = $arParams["SHOW_ALWAYS"];
	$arResult["NavTitle"] = $arParams["NAV_TITLE"];
	$arResult["NavRecordCount"] = $dbresult->NavRecordCount;
	$arResult["NavPageCount"] = $dbresult->NavPageCount;
	$arResult["NavPageNomer"] = $dbresult->NavPageNomer;
	$arResult["NavPageSize"] = $dbresult->NavPageSize;
	$arResult["bShowAll"] = $dbresult->bShowAll;
	$arResult["NavShowAll"] = $dbresult->NavShowAll;
	$arResult["NavNum"] = $dbresult->NavNum;
	$arResult["bDescPageNumbering"] = $dbresult->bDescPageNumbering;
	$arResult["add_anchor"] = $dbresult->add_anchor;
	$arResult["nPageWindow"] = $nPageWindow = $dbresult->nPageWindow;
	$arResult["bSavePage"] = (CPageOption::GetOptionString("main", "nav_page_in_session", "Y")=="Y");
	$arResult["sUrlPath"] = GetPagePath(false, false);
	$arResult["NavQueryString"]= htmlspecialcharsbx(DeleteParam(array(
		"PAGEN_".$dbresult->NavNum,
		"SIZEN_".$dbresult->NavNum,
		"SHOWALL_".$dbresult->NavNum,
		"PHPSESSID",
		"clear_cache",
		"bitrix_include_areas"
	)));
	$arResult['sUrlPathParams'] = $arResult['sUrlPath'].'?'.('' != $arResult['NavQueryString'] ? $arResult['NavQueryString'].'&' : '');

	if ($dbresult->bDescPageNumbering === true)
	{
		if ($dbresult->NavPageNomer + floor($nPageWindow/2) >= $dbresult->NavPageCount)
			$nStartPage = $dbresult->NavPageCount;
		else
		{
			if ($dbresult->NavPageNomer + floor($nPageWindow/2) >= $nPageWindow)
				$nStartPage = $dbresult->NavPageNomer + floor($nPageWindow/2);
			else
			{
				if($dbresult->NavPageCount >= $nPageWindow)
					$nStartPage = $nPageWindow;
				else
					$nStartPage = $dbresult->NavPageCount;
			}
		}

		if ($nStartPage - $nPageWindow >= 0)
			$nEndPage = $nStartPage - $nPageWindow + 1;
		else
			$nEndPage = 1;
	}
	else
	{
		if ($dbresult->NavPageNomer > floor($nPageWindow/2) + 1 && $dbresult->NavPageCount > $nPageWindow)
			$nStartPage = $dbresult->NavPageNomer - floor($nPageWindow/2);
		else
			$nStartPage = 1;

		if ($dbresult->NavPageNomer <= $dbresult->NavPageCount - floor($nPageWindow/2) && $nStartPage + $nPageWindow-1 <= $dbresult->NavPageCount)
			$nEndPage = $nStartPage + $nPageWindow - 1;
		else
		{
			$nEndPage = $dbresult->NavPageCount;
			if($nEndPage - $nPageWindow + 1 >= 1)
				$nStartPage = $nEndPage - $nPageWindow + 1;
		}
	}

	$arResult["nStartPage"] = $dbresult->nStartPage = $nStartPage;
	$arResult["nEndPage"] = $dbresult->nEndPage = $nEndPage;

	if ($dbresult->bDescPageNumbering === true)
	{
		$makeweight = ($dbresult->NavRecordCount % $dbresult->NavPageSize);
		$NavFirstRecordShow = 0;
		if($dbresult->NavPageNomer != $dbresult->NavPageCount)
			$NavFirstRecordShow += $makeweight;

		$NavFirstRecordShow += ($dbresult->NavPageCount - $dbresult->NavPageNomer) * $dbresult->NavPageSize + 1;

		if ($dbresult->NavPageCount == 1)
			$NavLastRecordShow = $dbresult->NavRecordCount;
		else
			$NavLastRecordShow = $makeweight + ($dbresult->NavPageCount - $dbresult->NavPageNomer + 1) * $dbresult->NavPageSize;

	}
	else
	{
		$NavFirstRecordShow = ($dbresult->NavPageNomer-1)*$dbresult->NavPageSize+1;

		if ($dbresult->NavPageNomer != $dbresult->NavPageCount)
			$NavLastRecordShow = $dbresult->NavPageNomer * $dbresult->NavPageSize;
		else
			$NavLastRecordShow = $dbresult->NavRecordCount;
	}

	$arResult["NavFirstRecordShow"] = $NavFirstRecordShow;
	$arResult["NavLastRecordShow"] = $NavLastRecordShow;

	if(!$arResult["NavShowAlways"])
	{
		if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
			return;
	}
	
	
	$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
	$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
	
	if($arResult["bDescPageNumbering"] === true):
		$bFirst = true;
		if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
			
			if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
				$bFirst = false;
				if($arResult["bSavePage"]):
	?>
				<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">1</a>
	<?
				else:
	?>
				<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
	<?
				endif;
				if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
	/*?>
				<span class="modern-page-dots">...</span>
	<?*/
	?>	
				<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2)?>">...</a>
	<?
				endif;
			endif;
		endif;
		do
		{
			$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;
			
			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
			<span class="<?=($bFirst ? "modern-page-first " : "")?>modern-page-current"><?=$NavRecordGroupPrint?></span>
	<?
			elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
	?>
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="<?=($bFirst ? "modern-page-first" : "")?>"><?=$NavRecordGroupPrint?></a>
	<?
			else:
	?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
				?> class="<?=($bFirst ? "modern-page-first" : "")?>"><?=$NavRecordGroupPrint?></a>
	<?
			endif;
			
			$arResult["nStartPage"]--;
			$bFirst = false;
		} while($arResult["nStartPage"] >= $arResult["nEndPage"]);
		
		if ($arResult["NavPageNomer"] > 1):
			if ($arResult["nEndPage"] > 1):
				if ($arResult["nEndPage"] > 2):
	/*?>
			<span class="modern-page-dots">...</span>
	<?*/
	?>
			<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nEndPage"] / 2)?>">...</a>
	<?
				endif;
	?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=$arResult["NavPageCount"]?></a>
	<?
			endif;
		
		endif; 
	
	else:
		$bFirst = true;
	
		if ($arResult["NavPageNomer"] > 1):
			
			if ($arResult["nStartPage"] > 1):
				$bFirst = false;
				if($arResult["bSavePage"]):
	?>
				<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a>
	<?
				else:
	?>
				<a class="modern-page-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
	<?
				endif;
				if ($arResult["nStartPage"] > 2):
	/*?>
				<span class="modern-page-dots">...</span>
	<?*/
	?>
				<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nStartPage"] / 2)?>">...</a>
	<?
				endif;
			endif;
		endif;
	
		do
		{
			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
			<span class="<?=($bFirst ? "modern-page-first " : "")?>modern-page-current"><?=$arResult["nStartPage"]?></span>
	<?
			elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
	?>
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="<?=($bFirst ? "modern-page-first" : "")?>"><?=$arResult["nStartPage"]?></a>
	<?
			else:
	?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
				?> class="<?=($bFirst ? "modern-page-first" : "")?>"><?=$arResult["nStartPage"]?></a>
	<?
	
			endif;
			$arResult["nStartPage"]++;
			$bFirst = false;
		} while($arResult["nStartPage"] <= $arResult["nEndPage"]);
		
		if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
			if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
				if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
	/*?>
			<span class="modern-page-dots">...</span>
	<?*/
	?>
			<a class="modern-page-dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2)?>">...</a>
	<?
				endif;
	?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>
	<?
			endif;
		endif;
	endif;
	
	if ($arResult["bShowAll"]):
		if ($arResult["NavShowAll"]):
	?>
			<a class="modern-page-pagen" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0"><?=GetMessage("nav_paged")?></a>
	<?
		else:
	?>
			<a class="modern-page-all" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1">Показать все</a>
	<?
		endif;
	endif
?>