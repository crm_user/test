var _LOADER;

function getRandomArrayElement() {
    let ar = ['up', 'down', 'left', 'right'];
    return ar[Math.floor(Math.random() * ar.length)]
}

$(document)
    .ajaxStart(function () {
        console.log('_ajaxStart(main script)');
        if (typeof _LOADER !== undefined && _LOADER.is(':hidden'))
        // _LOADER.show('fade', {}, 300);
            _LOADER.show('drop', {direction: getRandomArrayElement()}, 300);
    })

    .ajaxStop(function () {
        console.log('_ajaxStop(main script)');
        if (typeof _LOADER !== undefined && _LOADER.is(':visible'))
        // _LOADER.hide('fade', {}, 300);
            _LOADER.hide('drop', {direction: getRandomArrayElement()}, 300);
    })

    .ready(function () {
        _LOADER = $('#report-loader-container');

        $('#global-phone-search').find('input[name=phone]').mask('70000000000');
        $('#invoice-pdf-looking').find('input[name=invoice]').mask('0000000000');

    });

function ShowRecordMango(element) {
    $.ajax({
        type: "POST",
        data: {"cid": element.parent().attr('data-cid')},
        url: '/ajax/mango/get_record.php',
        dataType: 'json',
        timeout: 55000,
        success: function (result) {
            if (result.success) {
                element.parent().append(result.item);
                element.remove();
            }
            else if (result.error)
                alert(result.msg)
            else
                alert('Ошибка : неизвестный ответ сервера');
        },
        error: function () {
            alert('Ошибка сервер. Обратитесь к администратору');
        }
    });
}