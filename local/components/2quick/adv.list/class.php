<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Application,
    Bitrix\Main\Loader,
    \Bitrix\Iblock\Component\Tools;


Loader::includeModule('iblock');
Loader::includeModule("highloadblock");
class AdvListComponent extends CBitrixComponent
{
    public static $pagintaionStr;

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    private function getFiles(){

        $block = HL\HighloadBlockTable::getById($this->arParams['HL_BLOCK_ID']);

        if ($hlblock = $block->fetch()) {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $res = $entity_data_class::getList(array(
                    'filter' => array(),
                    'select' => array("*"),
                    'order' => array(
                        'ID' => 'desc'
                    ),
                )
            );

            while ($row = $res->fetch()) {
                foreach ($row['UF_ELEMENTS'] as $ELEMENT) {
                    $this->arResult['FILES'][$ELEMENT] = $row;
                }
            }
        }
        $arElements = array_keys($this->arResult['FILES']);
        $arElements = array_unique($arElements);

        return $arElements;
    }

    /**
     *
     */
    private function getCitys(){
        $this->arCity = array();
        $getOurPvzCities = City::getOurPvzCities();
        foreach ($getOurPvzCities as $ourCityInfo) {
            $this->arResult['CITYS'][$ourCityInfo['ID']] = $ourCityInfo;
        }
    }

    /**
     *
     */
    private function getDirections(){
        $this->arDirection = array();
        $except = array(
            false,//без категории
            128,//диагностика (после ремонта)
            //129,//комплектующие для микро
            115,//запчасти
        );
        $sectionList = getLinearSectionList(Product::IBLOCK_ID, $except);
        $sectionList = generateLinearSectionListName($sectionList);

        foreach ($sectionList as $sectionInfo) {
            if (isset($sectionInfo['PARENT_NAMES']) && !empty($sectionInfo['PARENT_NAMES'])) {
                $sectionInfo['NAME'] = implode(' >> ', $sectionInfo['PARENT_NAMES']) . ' >> ' . $sectionInfo['NAME'];
            }
            $this->arResult['DIRECTIONS'][$sectionInfo['ID']] = $sectionInfo;
        }
    }

    /**
     *
     */
    private function getTypes(){
        $res = CIBlockElement::GetList(
            Array("NAME" => "ASC"),
            Array('IBLOCK_ID' => $this->arParams['TYPES_IBLOCK_ID']),
            false,
            false,
            Array(
                'ID',
                'NAME',
                'CODE',
                'ACTIVE',
                'PROPERTY_COL_CITY',
                'PROPERTY_COL_DATE',
                'PROPERTY_COL_PRICE'
            )
        );
        while ($element = $res->GetNext()) {
            $this->arResult['TYPES'][$element['ID']] = $element;
        }
    }

    /**
     * @param $data
     * @return bool
     */
    private function setFilter($data){
        $result = false;
        if(!empty($data)){
            $result = true;

            if(!empty($data['date_start'])) {
                $this->arResult['FILTER']['>=UF_DATE'] = date('d.m.Y 00:00:00',strtotime($data['date_start'])) ;
            }
            if(!empty($data['date_end'])) {
                $this->arResult['FILTER']['<=UF_DATE'] = date('d.m.Y 23:59:59',strtotime($data['date_end'])) ;
            }
            if(!empty($data['direction'])) {
                $this->arResult['FILTER']['UF_DIRECTION'] = $data['direction'];
            }
            if(!empty($data['type'])) {
                $this->arResult['FILTER']['UF_ADV'] = $data['type'];
            }
            if(!empty($data['city'])) {
                $this->arResult['FILTER']['UF_CITY'] = $data['city'];
            }
        }
        return $result;
    }

    /**
     * @param $arElements
     * @return object
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    private function getItems($arElements){
        $result = [];
        if(!empty($arElements)){
            $block = HL\HighloadBlockTable::getById($this->arParams['HL_BLOCK_ID_ADV']);
            if ($hlblock = $block->fetch()) {
                $arFilter['ID'] = $arElements;
                if(!empty($this->arResult['FILTER'])){
                    $arFilter = array_merge($arFilter,$this->arResult['FILTER']);
                }
                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                $entity_data_class = $entity->getDataClass();

                $res = $entity_data_class::getList(array(
                        'filter' => $arFilter,
                        'select' => array("*"),
                        'order' => array(
                            'ID' => 'asc'
                        ),
                    )
                );
                $result = self::makePagination($res);
            }
        }
        return $result;
    }

    /**
     * 
     */
    private function getResult()
    {
            if(!empty($_GET['filter'])){
                $this->setFilter($_GET['filter']);
            }
            $arElements = $this->getFiles();




            $this->arResult['ITEMS'] = $this->getItems($arElements)->arResult;
            $this->arResult['PAGER'] = self::$pagintaionStr;


            $this->getTypes();
            $this->getDirections();
            $this->getCitys();


        
            $request = Application::getInstance()->getContext()->getRequest();
            $uriString = array_filter(explode("/", $request->getRequestedPage()), function ($var) {
                if ($var != 'index.php') {
                    return $var;
                } else {
                    return null;
                }
            }
            );

        if (count($uriString) > 1) {
            Tools::process404(
                ""
                , "Y"
                , "Y"
                , "Y"
                , $this->arParams["FILE_404"]
            );
        }

    }

    public static function makePagination($res)
    {
        $rows = array();
        while ($row = $res->fetch()) {
            $rows[] = $row;
        }
        $res = new \CDBResult;
        $res->InitFromArray($rows);

        $intItemPerPage = (!empty($_GET['SIZEN_2'])?$_GET['SIZEN_2']:10);
        $intCurPage = (!empty($_GET['PAGEN_2'])?$_GET['PAGEN_2']:1);


        $res->navStart($intItemPerPage,false,$intCurPage );

        self::$pagintaionStr = $res->GetPageNavStringEx($navComponentObject, '', 'round',false);

        return $res;
    }

    public static function getPagination()
    {
        return self::$pagintaionStr;
    }

    /**
     * @return mixed|void
     */
    public function executeComponent()
    {

        $this->getResult();
        $this->includeComponentTemplate();

    }

}