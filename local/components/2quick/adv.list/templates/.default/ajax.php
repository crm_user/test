<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Sale;
use Bitrix\Highloadblock as HL;
Loader::includeModule("highloadblock");

global $USER;
$request = Application::getInstance()->getContext()->getRequest();
$arResult = array();
if($request->isPost()) {
    $action = $request->getPost('action');
    switch($action)
    {
        case 'changePrice':
			$id = $request->getPost('id');
			$arData = $request->getPost('data');

            $hlbl = 45;
            $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $result = $entity_data_class::update($id, $arData);

          	echo json_encode($arData);
            break;
    }
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");