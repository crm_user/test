<?
$MESS["EMPTY_DIRECTION"] = "Выберите направление";
$MESS["EMPTY_ADV"] = "Выберите тип рекламы";
$MESS["EMPTY_FILE"] = "Прикрепите файл";
$MESS["CHOSEN_THE_SAME"] = "Выбранный вариант уже есть в таблице";
$MESS["EMPTY_IS_NOT_CORRECT"] = "Ваш файл содержит некорректные данные";
$MESS["CANNOT_FIND_CITY"] = "Не найден столбец города";
$MESS["CANNOT_FIND_DATE"] = "Не найден столбец даты";
$MESS["CANNOT_FIND_PRICE"] = "Не найден столбец цен";
$MESS["EMPTY_HLBLOCK"] = "Не найден ID HLBlockа";
$MESS["EMPTY_EDIT_NAME"] = "Имя не должно быть пустое";
$MESS["EMPTY_EDIT_CODE"] = "Код не должен быть пустым";
?>