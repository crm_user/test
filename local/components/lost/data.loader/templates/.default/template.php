<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data" id="adv-file-loader">
    <h4>Загрузка расходов</h4>
    <input type="hidden" name="download" value="Y">

    <? if ($arResult['WRONG_CITY'] != 'Y'): ?>
        <input type="hidden" name="correct_city" value="">
    <? endif; ?>
	<input type="hidden" name="TQ_CURRENT_FILE" value="<?=$arResult['CURRENT_FILE']['ID']?>">
    <? if (!empty($arResult['ERRORS']) && is_array($arResult['ERRORS']) && $_REQUEST['download'] == 'Y') {
//        pre($arResult['ERRORS']);
        $arResult['ERRORS'] = array_unique($arResult['ERRORS']);
        foreach ($arResult['ERRORS'] as $error)
            pre($error, '! ВНИМАНИЕ');
    } ?>
    <? if (!empty($_SESSION['WRONG_DATA'])): ?>
        <input type="hidden" name="direction" value="<?= $_REQUEST['direction'] ?>">
        <input type="hidden" name="adv" value="<?= $_REQUEST['adv'] ?>">
        <table cellpadding="3" cellspacing="0" border="1" width="100%">
            <tr>
                <? foreach ($_SESSION['WRONG_DATA']['HEADER'] as $key => $v): ?>
                    <? foreach ($v as $k => $header): ?>
                        <td>
                            <?= $header ?>:
                        </td>
                    <? endforeach; ?>
                <? endforeach; ?>
            </tr>
            <tr>
                <? foreach ($_SESSION['WRONG_DATA']['HEADER'] as $key => $v): ?>
                    <? foreach ($v as $k => $header): ?>
                        <td>
                            <b style="color: red"><?= $_SESSION['WRONG_DATA']['BODY'][$key][$k] ?></b>
                        </td>
                    <? endforeach; ?>
                <? endforeach; ?>
            </tr>
        </table>

    <? endif; ?>
    <table cellpadding="3" cellspacing="0" border="1" width="100%">
        <tr>
	        <?if(!$_SESSION['FILE_DATA_CONTINUE']){?>
            <?= $arResult['completed']; ?>
	        <?}?>
            <? if ($_SESSION['FILE_DATA_CONTINUE']): ?>
		        <td>
			        <input type="hidden" name="direction" value="<?= $_REQUEST['direction'] ?>">
			        <input type="hidden" name="adv" value="<?= $_REQUEST['adv'] ?>">
			        Времени не хватило, необходимо еще разок! Система автоматически отправит запрос.
			        <script>$(document).ready(function () {
                            $('#adv-file-loader').find('[type=submit]').trigger('click').hide(300);
                        });</script>
			        <br/><?= $arResult['completed']; ?>
		        </td>
            <? elseif ($arResult['WRONG_CITY'] != 'Y'): ?>
		        <td>Направление:<br/>
			        <select name="direction">
				        <option value="">Выбрать направление</option>
                        <? foreach ($arResult['DIRECTIONS'] as $id => $direction): ?>
					        <option
							        value="<?= $id ?>"<? if ($id == $arResult['CHECK_DIRECTION_IS']): ?> selected<? endif; ?>><?= $direction ?></option>
                        <? endforeach; ?>
			        </select>
		        </td>
		        <td>Реклама:<br/>
			        <select name="adv">
				        <option value="">Выбрать рекламу</option>
                        <? foreach ($arResult['ITEMS'] as $id => $item): ?>
					        <option
							        value="<?= $item['ID'] ?>"<? if ($item['ID'] == $arResult['CHECK_TYPE_IS']): ?> selected<? endif; ?>><?= $item['NAME'] ?></option>
                        <? endforeach; ?>
			        </select>
		        </td>
		        <td>
			        <input type="file" name="file">
		        </td>
            <? else: ?>
		        <td>Данный город не найден в базе данных, выберите новый из списка, или пропустите этап:<br/>
			        <select name="correct_city">
				        <option value="skipp">Пропустить этап</option>
                        <? foreach ($arResult['CITIES'] as $id => $name): ?>
					        <option value="<?= $id ?>"><?= $name ?></option>
                        <? endforeach; ?>
			        </select>
		        </td>
            <? endif; ?>
            <td>
                <input type="submit" value="Обработать">
            </td>
        </tr>
    </table>
</form>
<br/>
<br/>
<form action="" method="POST" enctype="multipart/form-data" id="edit_form">
    <h4>Тип контекстной рекламы</h4>
    <input type="hidden" name="edit_form" value="Y">
    <? if (!empty($arResult['ERRORS']) && is_array($arResult['ERRORS']) && $_REQUEST['edit_form'] == 'Y') {
        pre($arResult['ERRORS']);
//        $arResult['ERRORS'] = array_unique($arResult['ERRORS']);
//        foreach ($arResult['ERRORS'] as $error)
//            pre($error, '! ВНИМАНИЕ');
    }
    ?>
    <table style="text-align: center;" cellpadding="3" cellspacing="0" border="1">
        <tr>
            <td rowspan="2">Активность</td>
            <td rowspan="2">#</td>
            <td rowspan="2">Название</td>
            <td rowspan="2">Код</td>
            <td colspan="3">Название колонок в файле для:</td>
        </tr>
        <tr>
            <td>Города</td>
            <td>Даты</td>
            <td>Цены</td>
        </tr>
        <? foreach ($arResult['ITEMS'] as $key => $item):
            $active = 'Y';
            if (isset($_POST['ITEMS'][$key]) && $_POST['ITEMS'][$key]['ACTIVE'] != 'on') $active = 'N';
            elseif (!isset($_POST['ITEMS'][$key])) $active = $item['ACTIVE'];
            ?>
            <tr>
                <td>
                    <input type="checkbox"
                           name="ITEMS[<?= $item['ID'] ?>][ACTIVE]"<? if ($active == 'Y'): ?> checked<? endif; ?>>
                </td>
                <td>
                    <?= $item['ID'] ?>
                </td>
                <td>
                    <input type="text" name="ITEMS[<?= $item['ID'] ?>][NAME]"
                           value="<?= ($_POST['ITEMS'][$key]['NAME']) ? $_POST['ITEMS'][$key]['NAME'] : $item['NAME'] ?>">
                </td>
                <td>
                    <input type="text" name="ITEMS[<?= $item['ID'] ?>][CODE]"
                           value="<?= ($_POST['ITEMS'][$key]['CODE']) ? $_POST['ITEMS'][$key]['CODE'] : $item['CODE'] ?>">
                </td>
                <td>
                    <input type="text" name="ITEMS[<?= $item['ID'] ?>][PROPERTY_COL_CITY]"
                           value="<?= ($_POST['ITEMS'][$key]['PROPERTY_COL_CITY']) ? $_POST['ITEMS'][$key]['PROPERTY_COL_CITY'] : $item['PROPERTY_COL_CITY_VALUE'] ?>">
                </td>
                <td>
                    <input type="text" name="ITEMS[<?= $item['ID'] ?>][PROPERTY_COL_DATE]"
                           value="<?= ($_POST['ITEMS'][$key]['PROPERTY_COL_DATE']) ? $_POST['ITEMS'][$key]['PROPERTY_COL_DATE'] : $item['PROPERTY_COL_DATE_VALUE'] ?>">
                </td>
                <td>
                    <input type="text" name="ITEMS[<?= $item['ID'] ?>][PROPERTY_COL_PRICE]"
                           value="<?= ($_POST['ITEMS'][$key]['PROPERTY_COL_PRICE']) ? $_POST['ITEMS'][$key]['PROPERTY_COL_PRICE'] : $item['PROPERTY_COL_PRICE_VALUE'] ?>">
                </td>
            </tr>
        <? endforeach; ?>
        <tr>
            <td colspan="2">
                <input type="button" id="new" value="Новый">
            </td>
            <td colspan="5" align="right">
                <input type="submit" value="Сохранить">
            </td>
        </tr>
    </table>
</form>
<script>
    window.onload = setNewBtnEvent;
    BX.addCustomEvent('onAjaxSuccess', setNewBtnEvent);

    function setNewBtnEvent() {
        var button = document.querySelector('#new');
        button.addEventListener('click', function () {
            var table = document.querySelector('#edit_form table tbody');
            var tr = document.createElement('tr');
            var count = document.querySelectorAll('#edit_form table tr').length;
            var id = 'new' + count;
            tr.innerHTML = '<td><input type="checkbox" name="ITEMS[' + id + '][ACTIVE]" checked></td>'
                + '<td>#</td>'
                + '<td><input type="text" name="ITEMS[' + id + '][NAME]"></td>'
                + '<td><input type="text" name="ITEMS[' + id + '][CODE]"></td>'
                + '<td><input type="text" name="ITEMS[' + id + '][PROPERTY_COL_CITY]"></td>'
                + '<td><input type="text" name="ITEMS[' + id + '][PROPERTY_COL_DATE]"></td>'
                + '<td><input type="text" name="ITEMS[' + id + '][PROPERTY_COL_PRICE]"></td>';
            table.insertBefore(tr, table.children[count - 1]);
        });
    }
</script>